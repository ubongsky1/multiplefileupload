﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MultipleUpload.Data;
using MultipleUpload.Models;

namespace MultipleUpload.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ApplicationDbContext _context;

        public HomeController(IHostingEnvironment hostingEnvironment, ApplicationDbContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult FileUpload()
        {
           

            return View();
        }

        [HttpPost]
        public IActionResult FileUpload(ImageVm model)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = null;
                if (model.Photos != null && model.Photos.Count > 0)
                {
                    foreach (IFormFile photo in model.Photos)
                    {
                        string UploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "UploadedEmployeePhotos");
                        uniqueFileName = Guid.NewGuid().ToString() + "_" + photo.FileName;
                        string FilePath = Path.Combine(UploadsFolder, uniqueFileName);
                        photo.CopyTo(new FileStream(FilePath, FileMode.Create));
                        Image image = new Image()
                        {
                            Id = model.Id,
                            Photos = uniqueFileName
                        };
                        _context.Add(image);
                    }
                    
                }
              
                _context.SaveChanges();
                ViewBag.Mess = "file uploaded successfully";

                return RedirectToAction(nameof(FileUpload));
            }

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
